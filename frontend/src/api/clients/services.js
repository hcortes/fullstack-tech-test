import http from "./index";

class ClientsService {
    getAll() {
      return http.get("/client");
    }
    create(data) {
      return http.post("/client", data);
    }
    findByTitle(name) {
      return http.get(`/client?name=${name}`);
    }
  }
  export default new ClientsService();