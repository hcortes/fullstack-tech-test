import React from "react";
import { Routes, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import AddClient from "./views/add-clients";
import ClientList from "./views/clients-list";

const App = () => {
  return (
      <div>
        <h1>EVPro Full-stack Test</h1>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <a href="/clients" className="navbar-brand">
            Clients
          </a>
          <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to={"/clients"} className="nav-link">
                Clients
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/add"} className="nav-link">
                Add
              </Link>
            </li>
          </div>
        </nav>
        <div className="container mt-3">
          <Routes>
            <Route exact path="/" element={<ClientList/>} />
            <Route exact path="/clients" element={<ClientList/>} />
            <Route exact path="/add" element={<AddClient/>} />
          </Routes>
        </div>
      </div>
  );
};

export default App;
