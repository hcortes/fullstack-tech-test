import React, { Component } from "react";
import ClientsService from "../api/clients/services";
import { Link } from "react-router-dom";
export default class ClientsList extends Component {
  constructor(props) {
    super(props);
    this.retrieveClients = this.retrieveClients.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveClient = this.setActiveClient.bind(this);
    this.state = {
      clients: [],
      currentClient: null,
      currentIndex: -1,
    };
  }
  componentDidMount() {
    this.retrieveClients();
  }
  retrieveClients() {
    ClientsService.getAll()
      .then((response) => {
        this.setState({
          clients: response.data,
        });
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  }
  refreshList() {
    this.retrieveClients();
    this.setState({
      currentClient: null,
      currentIndex: -1,
    });
  }
  setActiveClient(client, index) {
    this.setState({
      currentClient: client,
      currentIndex: index,
    });
  }
  render() {
    const { clients, currentClient, currentIndex } = this.state;
    return (
      <div className="list row">
        <div className="col-md-6">
          <h4>Clients List</h4>
          <ul className="list-group">
            {clients &&
              clients.map((client, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveClient(client, index)}
                  key={index}
                >
                  {client.name}
                </li>
              ))}
          </ul>
        </div>
        <div className="col-md-6">
          {currentClient ? (
            <div>
              <h4>Client</h4>
              <div>
                <label>
                  <strong>Name:</strong>
                </label>{" "}
                {currentClient.name}
              </div>
              <div>
                <label>
                  <strong>Email:</strong>
                </label>{" "}
                {currentClient.email}
              </div>
              <div>
                <label>
                  <strong>Company:</strong>
                </label>{" "}
                {currentClient.company}
              </div>
              <div>
                <label>
                  <strong>Created Date:</strong>
                </label>{" "}
                {new Intl.DateTimeFormat("en-GB", {
                  year: "numeric",
                  month: "long",
                  day: "2-digit",
                }).format(new Date(currentClient.createdAt))}
              </div>
            </div>
          ) : (
            <div>
              <br />
              <p>Please click on a Client...</p>
            </div>
          )}
        </div>
      </div>
    );
  }
}
