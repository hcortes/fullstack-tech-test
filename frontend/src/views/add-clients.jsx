import React, { Component } from "react";
import ClientsService from "../api/clients/services";
export default class AddClient extends Component {
  constructor(props) {
    super(props);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangeCompany = this.onChangeCompany.bind(this);
    this.saveClient = this.saveClient.bind(this);
    this.newClient = this.newClient.bind(this);
    this.state = {
      id: null,
      name: "",
      email: "",
      company: "",
      published: false,
      submitted: false,
    };
  }
  onChangeName(e) {
    this.setState({
      name: e.target.value,
    });
  }
  onChangeEmail(e) {
    this.setState({
      email: e.target.value,
    });
  }
  onChangeCompany(e) {
    this.setState({
      company: e.target.value,
    });
  }
  saveClient() {
    var data = {
      name: this.state.name,
      email: this.state.email,
      company: this.state.company,
    };
    ClientsService.create(data)
      .then((response) => {
        this.setState({
          id: response.data.id,
          name: response.data.name,
          email: response.data.email,
          company: response.data.company,
          published: response.data.published,
          submitted: true,
        });
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  }
  newClient() {
    this.setState({
      id: null,
      name: "",
      email: "",
      company: "",
      published: false,
      submitted: false,
    });
  }
  render() {
    return (
        <div className="submit-form">
          {this.state.submitted ? (
            <div>
              <h4>You submitted successfully!</h4>
              <button className="btn btn-success" onClick={this.newClient}>
                Add
              </button>
            </div>
          ) : (
            <div>
              <div className="form-group">
                <label htmlFor="name">Name</label>
                <input
                  type="text"
                  className="form-control"
                  id="name"
                  required
                  value={this.state.name}
                  onChange={this.onChangeName}
                  name="name"
                />
              </div>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input
                  type="text"
                  className="form-control"
                  id="email"
                  required
                  value={this.state.email}
                  onChange={this.onChangeEmail}
                  name="email"
                />
              </div>
              <div className="form-group">
                <label htmlFor="company">Company</label>
                <input
                  type="text"
                  className="form-control"
                  id="company"
                  required
                  value={this.state.company}
                  onChange={this.onChangeCompany}
                  name="company"
                />
              </div>
              <button onClick={this.saveClient} className="btn btn-success">
                Save
              </button>
            </div>
          )}
        </div>
      );
  }
}
