const express = require("express");
const cors = require("cors");
const clients = require("./routes/clients");

const app = express();

var corsOptions = {
  origin: "http://localhost:3000"
};
app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const db = require("./config/db");
db.mongoose
  .connect(db.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Connected!");
  })
  .catch((err) => {
    console.log("Error connecting to the db!", err);
    process.exit();
  });

app.use("/client", clients);

app.listen(3001, () => {
  console.log("Server started on port 3001");
});
