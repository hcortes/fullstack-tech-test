const express = require("express");
const router = express.Router();
const clients = require("../controllers/clients");

router.get("/", clients.findAll);

router.post("/", clients.create);

module.exports = router;
