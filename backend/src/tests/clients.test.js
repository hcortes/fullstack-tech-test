const mockingoose = require("mockingoose");
const clientModel = require("../models/clients");

const { findAll } = require("../controllers/clients");

describe("Clients service", () => {
  describe("Find clients", () => {
    it("should return the list of clients", async () => {
      mockingoose(clientModel).toReturn(
        [
          {
            _id: "622938315edce937f498f1ed",
            name: "Heriberto Cortes",
            email: "hcortesb@gmail.com",
            company: "Test",
            createdAt: "2022-03-09T23:28:49.791Z",
            updatedAt: "2022-03-09T23:28:49.791Z",
            __v: 0,
          },
        ],
        "find"
      );
      const results = await findAll();
      expect(results[0].name).toBe("Heriberto Cortes");
    });
  });
});
