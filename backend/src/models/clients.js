module.exports = (mongoose) => {
  const Client = mongoose.model(
    "client",
    mongoose.Schema(
      {
        name: String,
        email: String,
        company: String,
      },
      { timestamps: true }
    )
  );
  return Client;
};
