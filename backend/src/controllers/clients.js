const db = require("../config/db");
const Client = db.clients;

exports.create = (req, res) => {
  if (!req.body.name || !req.body.email || !req.body.company) {
    res.status(400).send({ message: "Missing parameters!" });
    return;
  }

  const client = new Client({
    name: req.body.name,
    email: req.body.email,
    company: req.body.company,
  });

  client
    .save(client)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "There's been an error.",
      });
    });
};

exports.findAll = (req, res) => {
  const name = req.query.name;
  let condition = name
    ? { name: { $regex: new RegExp(name), $options: "i" } }
    : {};
  Client.find(condition)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "There's been an error.",
      });
    });
};
